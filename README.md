# Partie 1 : Commandes avancées Git

1. J'ai cliqué sur le bouton Fork du projet et j'ai ensuite cloner le projet sur mon pc.

2. Tous les tests ne s'effectuent pas.

3. J'ai a la basé crée une branche "fix-function-fibonnacci"

4. J'ai effectué deux commit pour corriger le bogue : Un pour la correction du nom de la fonction de test et l'autre pour la correction de la fonction fibonnacci.

5. J'ai utilisé la commande `git rebase -i HEAD~2` et j'ai ajouté squash aux commit situé après le deuxieme pour qu'ils soient assemblé avec le premier commit. J'ai ensuite mis un nouveau message.
   J'ai push cette branche sous le nom de "fix-function" et j'ai supprimé la branche "fix-function-fibonnacci"

6. J'ai utilisé `bisect start` puis `bisext good` et `bisect bad` (suivi chacun d'un commit) puis j'ai effectué les tests, git bisect good (ou bad) jusqu'a trouvé le mauvais commit.
7. J'ai trouvé le mauvais développeur qui est Guillaume Bernard.

8. Je me suis mis sur la branche production et j'ai fais `git cherry-pick` suivi du sha du commit de patch

9. ///

10. Ce fichier correspond aux lignes qui ont été supprimée et ajouté avec le nom du commit et de la personne a l'origine des commit.
    <br> <br>

# Partie 2 : Collaboration de code avec GitLab

1. Mon binome à utiliser mon dépot pour la suite.
2. J'ai invité mon binome via Members. ⋅
3. Chacun va implémenter une des deux fonctions
4. J'aurais la branche anagramme et mon binome la branche pour travailler sur l'autre fonction.
5. Nous avons fait une demande de merge request via l'outil proposé par gitlab en précisant les branches origine et cible de la merge request.
6. J'ai fais réviser la branche de mon binome on lui précisant qu'il fallait regler les conflits avant que le merge soit approuvé.
7. Nous avons ensuite vérifié ensemble la correction et j'ai approuvé le merge
8. Nous avons fini par établir ensemble le fichier de contribution.
9. J'ai ensuite commit le fichier sur mon dépot

<br> <br>

# Partie 3 : Utilisation de pre-commit pour Python

Dans cette dernière partie, vous allez apprendre à utiliser pre-commit pour garantir la
qualité du code Python que vous avez déjà rédigé.
Vous avez déjà un projet Python en cours. Voici ce que vous devez faire :

1. `pip3 install pre-commit`
2. Voir fichier `.pre-commit-config.yaml`. J'ai ajouté la ligne : ` args: ["--profile", "black", "--filter-files"]`pour gérer la compatibilité entre black et isort.
3. J'ai changé le nom d'une fonction avec des majuscules pour faire echouer le test.
4. J'ai lancé le commande `pre-commit run --all-files`pour qu'il reformate mes fichiers automatiquement
5. J'ai commit toutes les modifications.
6. J'ai poussé les modifications.
