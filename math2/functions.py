"""
Fonctions mathématiques de base
"""
from typing import List


def sqrt(number: int, tolerance: float = 1e-10) -> int:
    if number < 0.0:
        raise ValueError("Square root not defined for negative numbers.")
    guess = number
    while abs(guess * guess - number) > tolerance:
        guess = (guess + number / guess) / 2
    return guess


def average(x: List[float | int]):
    return sum(x) / float(len(x)) if x else 0


from itertools import accumulate, chain
from operator import mul


# factorial :: Integer
def factorial(n):
    return list(accumulate(chain([1], range(1, 1 + n)), mul))[-1]


def pgcd(u: int, v: int):
    return pgcd(v, u % v) if v else abs(u)


def median(aray) -> int:
    srtd = sorted(aray)
    alen = len(srtd)
    return 0.5 * (srtd[(alen - 1) // 2] + srtd[alen // 2])


def pow(x, y):
    return float(x) ** float(y)


from typing import Collection


def fibonacci(until: float) -> Collection[int]:
    n1 = 0
    n2 = 1
    numbers = [n1, n2]
    for loop_count in range(2, until):
        next = n1 + n2
        numbers.append(loop_count)

        n1 = n2
        n2 = next
    return numbers


def fsum(x: list[float]) -> "Float":
    if len(x) > 0:
        return x[0] + fsum(x[1:])
    return 0


def crible_eratosthene(n: int) -> List[int]:
    primes = []
    sieve = [True] * (n + 1)

    for p in range(2, n + 1):
        if sieve[p]:
            primes.append(p)
            for i in range(p * p, n + 1, p):
                sieve[i] = False

    return primes


def anagrammes(texte: str) -> List[str]:
    mots = texte.split()
    anagrammes_list = []

    for i in range(len(mots)):
        for j in range(i + 1, len(mots)):
            if est_anagramme(mots[i], mots[j]):
                anagrammes_list.append((mots[i], mots[j]))

    return anagrammes_list
